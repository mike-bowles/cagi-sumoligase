__author__ = 'ubuntu'
names = ['ubc9_2grnOut.txt', 'ubc9_2O25Out.txt','ubc9_2PE6Out.txt', 'ubc9_2UYZOut.txt', 'ubc9_2VRROut.txt', 'ubc9_3UINOut2.txt', 'ubc9_3UIOOut2.txt']
inFile = 'output/ubc9_3UINOut2.txt'
fd = open(inFile)

lastLine = []
list = []
firstLine = True
lineInfo = 'start'
cnt = 0
for line in fd:
    if firstLine:
        firstLine = False
        continue
    lineList = line.rstrip('[]').rstrip().split(',')
    if not lineList[1:4] == lineInfo:
        if not lineInfo == 'start':
            cleanInfo = [x.replace('"', '') for x in lineInfo]
            cleanInfo.append(cnt)
            list.append(cleanInfo)
            cnt = 0
        lineInfo = lineList[1:4]
    else:
        cnt += 1

print len(list)
for elt in list:
    print elt[0], elt[1], elt[2], elt[3]