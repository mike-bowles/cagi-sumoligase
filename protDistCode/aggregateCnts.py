__author__ = 'ubuntu'
names = ['ubc9_2grnCnt.txt', 'ubc9_2PE6Cnt.txt', 'ubc9_2UYZCnt.txt', 'ubc9_2VRRCnt.txt',
         'ubc9_3UINCnt.txt', 'ubc9_3UIOCnt.txt']

files = []
for x in names:
    files.append(open('contactCts/' + x))

data = []
for obj in files:
    tempList = [line.replace("'", '').split() for line in obj]
    data.append(tempList)

agg = []
for i in range(1, 159):
    listTemp = []
    for datum in data:
        for aa in datum:
            if int(aa[2]) == i:
                listTemp.append(aa)
    if len(listTemp) > 0:
        cntSum = sum([int(x[3]) for x in listTemp])
        aggLine = [x for x in listTemp[0]]
        aggLine[3] = cntSum
        agg.append(aggLine)

for line in agg:
    print line

