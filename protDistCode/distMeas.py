__author__ = 'ubuntu'

from math import sqrt
import json

#Names: ubc9_2grn.txt, ubc9_2O25.txt, ubc9_2PE6.txt, ubc9_2UYZ.txt, ubc9_2VRR.txt, ubc9_3UIN.txt, ubc9_3UIO.txt

inFile = 'ubc9_3UIO.txt'
fd = open(inFile)
a1 = []
a2 = []

for line in fd:
    lineSplit = line.split()
    if lineSplit[4] == 'A' and lineSplit[0]=="ATOM":
        a1.append(lineSplit)
    elif lineSplit[0]=="ATOM":
        a2.append(lineSplit)

def dist(b, c):
    distSum = 0.0
    for i in [6,7,8]:
        x = float(b[i])
        y = float(c[i])
        distSum += (x - y) * (x - y)
    return sqrt(distSum)



print a1[0]
print a2[0]

thresh = 10.0

distStar = 100000.0
atomNum1 = 50000
atomNum2 = 50000

outList = []

for list1 in a1:
    for list2 in a2:
        d = dist(list1, list2)
        if d < thresh:
            distStar = d
            atomNum1 = list1[1]
            atomNum2 = list2[1]
            saveList = [d, list1[3], list1[4], list1[5], list2[3], list2[4], list2[5]]
            outList.append(saveList)

#print distStar, atomNum1, atomNum2
print len(outList)
outFile = open('output/ubc9_3UIOOut2.txt', 'w')
outFile.write(inFile)
outFile.write('\n')

for elt in outList:
    json.dump(elt, outFile)
    outFile.write('\n')
outFile.close()

#2.63820412402 711 1995
