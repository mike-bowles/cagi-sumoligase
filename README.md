# README #

* Quick summary

These are the documents for an analysis of Ube2I - a SUMO ligase
for the 2015 CAGI competition:

https://genomeinterpretation.org/content/4-SUMO_ligase
Entrez Gene ID: 7329
UniprotKB: http://www.uniprot.org/uniprot/P63279
PDB ID of Ube2I crystal structure: 1A3S
PDB IDs of Ube2I complex co-crystal structures: 1KPS, 2GRN, 2O25, 2PE6, 2UYZ, 2VRR, 3UIN, 3UIO, 3UIP, 4Y1L
* Versions will be fluid as this is a 2 month project
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

RESOURCES

TODOs:
papers on Sumo and SUMO ligase, in yeast and human
alignments and structures

(1) Here is the link to the yeast SUMO  SMT3 with links to the 3D structure database:
http://www.uniprot.org/uniprot/Q12306
(2) Found the conserved UBC9 UNiProt link in Yeast : http://www.uniprot.org/uniprot/P50623
(3)  Link to  159 protein interactions with the ligase   http://thebiogrid.org/31995

Structure Viewers:
http://www.rcsb.org/pdb/static.do?p=software/software_links/molecular_graphics.html

### Contribution guidelines ###
Only team members will be allowed to contribute - the contest will be done in mid November 2015

### Who do I talk to? ###

* Repo owner or admin : rtshigeta at yahoo.com